"use strict"

// I don't know if this is allowed, but eh. I can't find an api-type thing.

const request = require('request')
const cheerio = require('cheerio')
const q = require('bluebird')

function getDevicePageHTML(device) {
  let url = `https://dl.twrp.me/${device}/`

  return new q.Promise((res, rej) => request(url, (err, resp) => {
    if (err)
      return rej(err)

    if (resp.statusCode != 200)
      return rej(resp.statusCode)

    res(resp.body)
  }))
}

module.exports.getReleases = function(device) {
  return getDevicePageHTML(device).then(html => {
    let $ = cheerio.load(html)

    let links = $('body').find('.post article:nth-child(3) table tr')
    let list = Array.from(links)

    return q.map(list, tr => $(tr).find('td')).map(list => Array.from(list)).map(([linkbox, sizebox, datebox]) => ({
      date: new Date($(datebox).text()).getTime(),
      link: $(linkbox).text(),
      size: $(sizebox).text()
    }))

    .reduce((list, file) => {
      file.link.includes('installer') ? list.installers.push(file) : list.releases.push(file)
      return list
    }, {installers: [], releases: []})

    .then(list => {
      list.installers = list.installers.sort((a, b) => b.date - a.date)
      list.releases = list.releases.sort((a, b) => b.date - a.date)

      return list
    })
  })
}